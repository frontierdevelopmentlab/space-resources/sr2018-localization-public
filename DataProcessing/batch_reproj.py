from multiprocessing import Pool
from functools import partial
import sys
import glob
import cv2 as cv
import os
import numpy as np
import math

N_POOL = 24  # Number of parallel threads upon which to run reprojection script

def reproj_fn(output_dir, views):
    quiet_operation = True
    save_output = True
    check_reprojection_exists = True

    # camera intrinsic values
    specs0 = {'fov_y': None, 'fov_x': 90, 'camera_height': 2, 'camera_pitch': -15,
              'rover_pitch': 0, 'rover_roll': 0, 'camera_yaw' : 0, 
              'desired_resolution': 0.05, 'minimum_resolution' : None, 'output_region_size': 50,}
    specs1 = {'fov_y': None, 'fov_x': 90, 'camera_height': 2, 'camera_pitch': -15,
              'rover_pitch': 0, 'rover_roll': 0, 'camera_yaw': 90,
              'desired_resolution': 0.05, 'minimum_resolution' : None, 'output_region_size': 50,}
    specs2 = {'fov_y': None, 'fov_x': 90, 'camera_height': 2, 'camera_pitch': -15,
              'rover_pitch': 0, 'rover_roll': 0, 'camera_yaw': 180,
              'desired_resolution': 0.05, 'minimum_resolution' : None, 'output_region_size': 50,}
    specs3 = {'fov_y': None, 'fov_x': 90, 'camera_height': 2, 'camera_pitch': -15,
              'rover_pitch': 0, 'rover_roll': 0, 'camera_yaw': 270,
              'desired_resolution': 0.05, 'minimum_resolution' : None, 'output_region_size': 50,}

    # 4x rover views -> reprojection
    reproject_multiple_rectilinears_to_aerial(views, [specs0,specs1,specs2,specs3], output_dir, quiet_operation, save_output, check_reprojection_exists)



def reproject_multiple_rectilinears_to_aerial(image_filenames, specs, output_dir, quiet_operation=True, save_output=True, check_reprojection_exists=False):
    num_images = len(image_filenames)

    reproject_image = True
    total_image = []
    location_id = []
    try:
        if check_reprojection_exists is True:
            image = cv.imread(image_filenames[0], 0)
            location_id = get_location_id(image_filenames[0])

            image_path = join(image_filenames[0].split(os.sep)[:-1], os.sep)
            if quiet_operation is False:
                print("image_path: "+image_path)
            reprojection_filename = 're_%08i.png' % location_id
            if quiet_operation is False:
                print('checking for reprojection at '+os.path.join(output_dir, reprojection_filename))
            if os.path.isfile(os.path.join(output_dir, reprojection_filename)):
                reproject_image = False

        if reproject_image is True:
            for i in range(num_images):
                if not (i == 0 and check_reprojection_exists is True):
                    image = cv.imread(image_filenames[i], 0)

                image, warped_image = reproject_rectilinear_to_aerial(image, specs[i]['fov_x'], specs[i]['fov_y'],
                                                                    specs[i]['camera_height'], specs[i]['camera_pitch'],
                                                                    specs[i]['camera_yaw'],specs[i]['rover_pitch'],
                                                                    specs[i]['rover_roll'], specs[i]['desired_resolution'],
                                                                    specs[i]['minimum_resolution'], specs[i]['output_region_size'], quiet_operation)
                if i == 0:
                    total_image = np.array(warped_image)

                    midpoint = np.divide(np.subtract(warped_image.shape,1), 2.)
                    divider = np.linspace(warped_image.shape[0] - 1, 0, warped_image.shape[0])

                    divider0 = np.round(np.array(rotate(np.stack((divider, divider), axis=1), specs[i]['camera_yaw'], midpoint))).astype(int)
                    divider1 = np.round(np.array(rotate(np.stack((divider, divider), axis=1), specs[i]['camera_yaw']+90, midpoint))).astype(int)
                    divider0 = divider0[int(midpoint[0])-1:]
                    divider1 = divider1[int(midpoint[0])-1:]

                    image_mask = np.zeros(warped_image.shape, dtype=bool)
                    for i in range(len(divider0)):
                        image_mask[np.arange(np.min([divider0[i,1], divider1[i,1]]), np.max([divider0[i,1],divider1[i,1]])+1), divider0[i,0]] = True
                        image_mask[divider0[i, 1], np.arange(np.min([divider0[i, 0], divider1[i, 0]]), np.max([divider0[i, 0], divider1[i, 0]])+1)] = True
                else:
                    warped_image = np.array(warped_image)

                    midpoint = np.divide(np.subtract(warped_image.shape,1), 2.)
                    divider = np.linspace(warped_image.shape[0] - 1, 0, warped_image.shape[0])

                    divider0 = np.round(np.array(rotate(np.stack((divider, divider), axis=1), specs[i]['camera_yaw'], midpoint))).astype(int)
                    divider1 = np.round(np.array(rotate(np.stack((divider, divider), axis=1), specs[i]['camera_yaw']+90, midpoint))).astype(int)
                    divider0 = divider0[int(midpoint[0])-1:]
                    divider1 = divider1[int(midpoint[0])-1:]

                    image_mask = np.zeros(warped_image.shape, dtype=bool)
                    for i in range(len(divider0)):
                        image_mask[np.arange(np.min([divider0[i,1], divider1[i,1]]), np.max([divider0[i,1],divider1[i,1]])+1), divider0[i,0]] = True
                        image_mask[divider0[i, 1], np.arange(np.min([divider0[i, 0], divider1[i, 0]]), np.max([divider0[i, 0], divider1[i, 0]])+1)] = True

                    total_image[image_mask] = warped_image[image_mask]

            if save_output:
                cv.imwrite(os.path.join(output_dir,"re_%08i.png" % location_id), total_image)
                if not quiet_operation:
                    print(os.path.join(output_dir,'re_%08i.png' % location_id))
    except Exception as e:
        print('Error occured for filenames: ')
        print(image_filenames)
        return None

    return total_image, location_id




def reproject_rectilinear_to_aerial(img, fov_x=90, fov_y=90, camera_height=1, camera_pitch=0, camera_yaw=0, rover_pitch=0,
                                    rover_roll=0, desired_resolution=0.05, minimum_resolution=None, output_region_size=None, quiet_operation=True):
    # Calculate displacement of pixels in first-person perspective for top-down aerial view
    # Outputs:
    #    img: Rectilinear image corrected to remove pixels above horizon (RGB or Greyscale)
    #    warped_img: Aerial image with minimum pixel resolution equal to desired_resolution (RGB or Greyscale)
    # Inputs:
    #    img: Rectilinear image (RGB or Greyscale)
    #    fov_x: Horizontal field of view angles (degrees)
    #    fov_y: Vertical field of view angles (degrees)
    #    camera_y: Height of camera from ground-level (meters)
    #    camera_pitch: Pitch angle of camera. (degrees) 0 = parallel, 90 = zenith, -90 = nadir.
    #    camera_yaw: Yaw angle of camera. (degrees) 0 = North, 90 = East, -90 = West.
    #    rover_pitch: Displacement angle of rover front with respect to tangent (0 horizon)
    #    rover_roll: Displacement angle of rover left side with respect to tangent (0 horizon)
    #    desired_resolution: meteres per pixel

    fov_x_pix_angle, fov_y_pix_angle = calculate_rectilinear_pixel_angle(img, fov_x, fov_y, camera_pitch, quiet_operation)
    img, fov_y_pix_angle = cut_horizon_from_image(img, fov_y_pix_angle, quiet_operation)
    dxdz, dz = calculate_rectilinear_pixel_displacement(fov_x_pix_angle, fov_y_pix_angle, camera_height, quiet_operation)
    dxdz, dz = filter_aerial_resolution(dxdz, dz, minimum_resolution, output_region_size, quiet_operation)
    #dxdz, dz = rotate_pixel_displacements(dxdz, dz, camera_yaw)
    dxdz, dz = rover_aerial_to_satellite_aerial(dxdz, dz, rover_pitch, rover_roll, quiet_operation)
    warped_img = warp_rectilinear_to_aerial(img, dxdz, dz, camera_yaw, desired_resolution, output_region_size, quiet_operation)
    #warped_img = rotate_aerial_image(warped_img, camera_yaw)

    return img, warped_img


def calculate_rectilinear_pixel_angle(img, fov_x, fov_y, camera_pitch, quiet_operation=True):
    # Calculate displacement of pixels in first-person perspective for top-down aerial view
    # Outputs:
    #    img: RGB or Greyscale
    #    dxdz: x displacement of pixel row, column
    #    dxdz: z displacement of pixel row
    # Inputs:
    #    img: RGB or Greyscale
    #    fov_x: Horizontal field of view angles (degrees)
    #    fov_y: Vertical field of view angles (degrees)
    #    camera_y: Height of camera from ground-level (meters)
    #    camera_pitch: Pitch angle of camera. (degrees) 0 = parallel, 90 = zenith, -90 = nadir.

    if not quiet_operation:
        print('ALERT: Calculating radians-per-pixel of horizontal and vertical fields of view: '+str(fov_x)+','+str(fov_y)+' (deg), for camera pitch: '+str(camera_pitch)+' (deg).')
    fov_height, fov_width = img.shape[:2]

    if fov_x == None:
        fov_x = 90.
    fov_x = (fov_x * math.pi) / 180.
    if fov_y == None:
        fov_y = 2 * np.arctan((float(fov_height) / float(fov_width)) * np.tan(fov_x / 2))
        # fov_y = (float(fov_height)/float(fov_width))*float(fov_x)
    else:
        fov_y = (fov_y * math.pi) / 180.

    camera_pitch = (camera_pitch * math.pi) / 180.

    rad_per_pix_y = float(fov_y) / float(fov_height)
    rad_per_pix_x = float(fov_x) / float(fov_width)

    fov_y_pix_angle = np.zeros([int(fov_height), 1])
    fov_y_pix_angle[:, 0] = np.add(
        np.subtract(np.multiply(np.linspace(fov_height - 0.5, 0.5, fov_height), rad_per_pix_y), (fov_y / 2)),
        camera_pitch)
    fov_x_pix_angle = np.zeros([1, fov_width])
    fov_x_pix_angle[0, :] = np.subtract(np.multiply(np.linspace(0.5, fov_width - 0.5, fov_width), rad_per_pix_x),
                                        (fov_x / 2))
    return fov_x_pix_angle, fov_y_pix_angle


def cut_horizon_from_image(img, fov_y_pix_angle, quiet_operation=True):
    if not quiet_operation:
        print('ALERT: Removing pixels above horizon from image.')
    horizon_point = np.min(np.where(fov_y_pix_angle <= 0)[0])
    fov_y_pix_angle = fov_y_pix_angle[horizon_point:]
    img = img[horizon_point:, :]
    return img, fov_y_pix_angle


def calculate_rectilinear_pixel_displacement(fov_x_pix_angle, fov_y_pix_angle, camera_height, quiet_operation=True):
    if not quiet_operation:
        print('ALERT: Calculating ground-plane pixel displacement from field of view radians-per-pixel and a camera height of '+str(camera_height)+' meters.')
    dz = np.multiply(float(camera_height), np.tan(fov_y_pix_angle + (math.pi / 2)))
    # calculates the distance of each horizontal line in the FOV image
    # assuming a flat plane parallel to the base of the rover

    dz2 = np.sqrt(np.add(np.power(dz, 2), np.power(camera_height, 2)))
    dxdz = np.dot(dz2, np.tan(fov_x_pix_angle))

    return dxdz, dz


def filter_aerial_resolution(dxdz, dz, minimum_resolution=None, output_region_size =None, quiet_operation=True):
    # Filter regions that do not meet upper bound of resolution
    # Outputs:
    #    img: RGB or Greyscale
    #    dxdz: x displacement of pixel row, column
    #    dxdz: z displacement of pixel row
    # Inputs:
    #    img: RGB or Greyscale
    #    dxdz: x displacement of pixel row, column
    #    dxdz: z displacement of pixel row
    #    desired_resolution: metres per pixel

    if output_region_size != None:
        if not quiet_operation:
            print('ALERT: Limiting reprojection tile to output region size: '+str(output_region_size)+' square meters.')
            print('WARNING: Minimum resolution parameter is being ignored.')
        dz[dz > (output_region_size/2.)] = np.inf
        dxdz[np.abs(dxdz) > (output_region_size/2.)] = np.inf
    elif minimum_resolution != None:
        if not quiet_operation:
            print('ALERT: Filtering reprojection tile to represent pixels of minimum resolution: ' + str(minimum_resolution) + ' meters.')
        ddz = dz[1:] - dz[0:-1]
        dz[np.where(np.abs(ddz) > minimum_resolution)[0]] = np.inf
        ddxdz = dxdz[:, 1:] - dxdz[:, 0:-1]
        mid_dx = int(np.floor(dxdz.shape[1] / 2))
        dxdz_nans = np.array(np.where(ddxdz > minimum_resolution))
        dxdz_nans[1, np.where(dxdz_nans[1] > mid_dx)] += 1
        dxdz[dxdz_nans[0], dxdz_nans[1]] = np.inf
    else:
        if not quiet_operation:
            print('ALERT: No filtering on minimum input resolution or limiting of output region size.')
            print('WARNING: Distance to horizon is '+str(dz[-1])+'meters. Output image may be very large.')

    return dxdz, dz


def rotate_pixel_displacements(dxdz, dz, angle, anchor=(0,0)):
    height, width = dxdz.shape
    dydz = np.repeat(dz.flatten(), width)
    dxdz = dxdz.flatten()
    points = np.stack((dxdz, dydz), axis=1)
    new_points = np.array(rotate(points, angle, anchor))
    dxdz = new_points[:,0].reshape([height, width])
    dz = new_points[::width,1]
    return dxdz, dz


def rover_aerial_to_satellite_aerial(dxdz, dz, rover_pitch, rover_roll, quiet_operation=True):
    # Convert pixel displacements from rover top-down to satellite aerial
    # Inputs:
    #    dxdz: x displacement of pixel row, column
    #    dxdz: z displacement of pixel row
    #    rover_pitch: Displacement angle of rover front with respect to tangent (0 horizon)
    #    rover_roll: Displacement angle of rover left side with respect to tangent (0 horizon)
    # Outputs:
    #    dxdz: x displacement of pixel row, column
    #    dxdz: z displacement of pixel row
    if not quiet_operation:
        print('ALERT: Adjusting reprojection to account for rover pitch and roll: '+str(rover_pitch)+', '+str(rover_roll)+' (deg).')

    rover_pitch = (rover_pitch * math.pi) / 180.
    rover_roll = (rover_roll * math.pi) / 180.

    dxdz = np.multiply(dxdz, np.cos(rover_roll))
    dz = np.multiply(dz, np.cos(rover_pitch))
    return dxdz, dz


def warp_rectilinear_to_aerial(img, dxdz, dz, rotation_angle, desired_resolution, output_region_size=None, quiet_operation=True):
    # Convert rectilinear image to aerial view via pixel displacements
    # Inputs:
    #    img: RGB or Greyscale (horizon adjusted)
    #    dxdz: x displacement of pixel row, column (meters)
    #    dxdz: z displacement of pixel row (meters)
    #    desired_resolution: output meter-per-pixel resolution
    # Outputs:
    #    warped_img:

    valid_y = np.where(dz.flatten() != np.inf)
    y_ind = [np.max(valid_y), np.max(valid_y),
             np.min(valid_y), np.min(valid_y)]
    y = [dz[y_ind[0]][0], dz[y_ind[1]][0], dz[y_ind[2]][0], dz[y_ind[3]][0]]

    valid_x = (dxdz != np.inf)
    x_ind = [np.argmax(valid_x[y_ind[0]]), np.max(np.where(valid_x[y_ind[1]])),
             np.argmax(valid_x[y_ind[2]]), np.max(np.where(valid_x[y_ind[3]]))]

    x = [dxdz[y_ind[0], x_ind[0]], dxdz[y_ind[1], x_ind[1]], dxdz[y_ind[2], x_ind[2]],
         dxdz[y_ind[3], x_ind[3]]]

    if output_region_size != None:
        output_resolution = (
            int(np.ceil((np.divide(output_region_size,desired_resolution)))),
            int(np.ceil((np.divide(output_region_size,2*desired_resolution))))) #height is half of width.
        if not quiet_operation:
            print('ALERT: Scaling reprojected tile to fit output region size of: '+str(output_region_size)+' meters for ' + str(desired_resolution) + ' meters per pixel.')
    else:
        output_resolution = (
            # int(np.ceil((np.max(x) - np.min([0., np.min(x)])) / desired_resolution)), int(np.ceil((np.max(y) - np.min([0., np.min(y)])) / desired_resolution)))
            int(np.ceil((np.max(x) - np.min([0., np.min(x)])) / desired_resolution)),
            int(np.ceil((np.max(y) - np.min([0., np.min(y)])) / desired_resolution)))
        if not quiet_operation:
            print('ALERT: Scaling reprojected tile for desired resolution of: ' + str(desired_resolution) + ' meters per pixel.')

    square_resolution = np.max([np.max(output_resolution), np.min(output_resolution) * 2])

    x = np.subtract(x, np.min([0, np.min(x)]))
    x = np.divide(x, np.max(x))
    x = np.multiply(x, output_resolution[0])
    y = np.subtract(y, np.min([0, np.min(y)]))
    y = np.divide(y, np.max(y))
    y = np.multiply(y, output_resolution[1])
    y = np.subtract(square_resolution/2, y)
    if not quiet_operation:
        print('ALERT: Rotating output reprojection points by camera yaw of ' + str(rotation_angle) + ' (deg)')
    new_points = np.array(rotate(np.stack((x, y), axis=1), rotation_angle, (square_resolution/2, square_resolution/2)))
    x = new_points[:, 0]
    y = new_points[:, 1]

    if not quiet_operation:
        print('ALERT: Warping first-perspective to aerial reprojection.')
    # close left close right far left far right
    pts1 = np.float32([[x_ind[0], y_ind[0]], [x_ind[1], y_ind[1]], [x_ind[2], y_ind[2]],
                       [x_ind[3], y_ind[3]]])  # 0,0 is top left (above horizon)
    pts2 = np.float32([[x[0], y[0]], [x[1], y[1]], [x[2], y[2]], [x[3], y[3]]])


    M = cv.getPerspectiveTransform(pts1.astype('float32'), pts2.astype('float32'))

    try:
        warped_img = cv.warpPerspective(img, M, (square_resolution, square_resolution))
    except:
        print('ERROR: Something went wrong during reprojection! Warping terminated.')
        warped_img = None

    return warped_img


def rotate_aerial_image(img, rotation_angle):
    # Rotates aerial image via rotation_angle
    # Inputs:
    #    img: RGB or Greyscale (horizon adjusted)
    #    rotation_angle (degrees) 0 = no rotation, 90 = clockwise , -90 = West.
    #    dxdz: z displacement of pixel row (meters)s
    #    desired_resolution: output meter-per-pixel resolution
    # Outputs:
    #   warped_img:

    (aerial_height, aerial_width) = img.shape[:2]

    # calculate rotation and extract cos and sin components
    M = cv.getRotationMatrix2D((aerial_width * 0.5, aerial_height * 0.5), -rotation_angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # new bounds
    width = int((aerial_height * sin) + (aerial_width * cos))
    height = int((aerial_height * cos) + (aerial_width * sin))

    # translate
    M[0, 2] += (width / 2) - (aerial_width * 0.5)
    M[1, 2] += (height / 2) - (aerial_height * 0.5)

    # rotate
    return cv.warpAffine(img, M, (width, height))


def join(iterator, seperator):
    it = map(str, iterator)
    seperator = str(seperator)
    string = next(it, '')
    for s in it:
        string += seperator + s
    return string


def get_location_id(image_filename):
    """returns the location_id reference from a ground-view image_filename"""
    return int(image_filename.split(os.sep)[-1].split('_')[1])


def rotate(points, angle, anchor=(0, 0)):
    angle = (angle / 180.)*np.pi
    xform = make_rotation_transformation(angle, anchor)
    return [xform(p) for p in points]


def make_rotation_transformation(angle, origin=(0, 0)):
    cos_theta, sin_theta = np.cos(angle), np.sin(angle)
    x0, y0 = origin
    def xform(point):
        x, y = point[0] - x0, point[1] - y0
        return (x * cos_theta - y * sin_theta + x0,
                x * sin_theta + y * cos_theta + y0)
    return xform


if __name__ == '__main__':
    game_path = str(sys.argv[1])  # Location of MoonBench 'game' folder
    save_path = os.path.join(os.path.join(game_path, 'MyProject'), 'Saved')

    output_dir = os.path.join(save_path, 'Reprojections')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    for area in ['training', 'validation', 'testing']:
        image_path = os.path.join(os.path.join(save_path, 'Screenshots'), 'images_'+area)
        output_path = os.path.join(output_dir, 'images_'+area)
        if not os.path.isdir(output_path):
            os.mkdir(output_path)
        print('Input Directory: '+image_path)
        print('Output Directory: '+output_path)
        all_views = sorted(glob.glob('%s/*.png' % image_path))

        if len(all_views) > 0:
            num_sets = len(all_views)//4
            trim = len(all_views)%4
            print('Number of reprojections to generate: %i' % num_sets)
            if trim > 0:
                rov_views = np.array_split(all_views[:-trim], num_sets)
            else:
                rov_views = np.array_split(all_views, num_sets)

            # parallel
            func = partial(reproj_fn, output_path)
            p = Pool(N_POOL)
            p.map(func, rov_views)

    # serial
    #for i in range(num_sets):
    #    reproj_fn(output_dir, rov_views[i])
