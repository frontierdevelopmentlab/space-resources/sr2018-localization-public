# NASA Frontier Development Lab (FDL): <br /> Space Resources - Localization

<img src="https://wubenjamin.github.io/images/NASA_FDL/VR_test.png" alt="panorama"/>

### FDL LUNA Localization Dataset

The [NASA FDL](https://frontierdevelopmentlab.org/) Lunar UNreal Assets (LUNA) Localization Dataset was designed to test automated localization algorithms on planetary surfaces, such as the Moon or Mars, where GPS is unavailable. The goal is to accurately determine absolute location based on ground-perspective images taken from the planetary surface (e.g., by rovers such as [MER](https://mars.nasa.gov/mer/) and [MSL](https://mars.nasa.gov/msl/)) via matching them with top-down orbital maps taken from satellites (e.g., [LRO](https://lunar.gsfc.nasa.gov/), [MRO](https://mars.nasa.gov/mro/)). 

Due to a lack of georeferenced data from extraterrestrial surfaces, a virtual environment and synthetic dataset were generated using [Unreal Engine 4](https://www.unrealengine.com/en-US/) and [Moon Landscape v3.0](https://www.unrealengine.com/marketplace/en-US/slug/the-moon). The LUNA Localization Dataset contains imagery for over 600,000 unique locations, summarized below:

<!-- Training Environment <br /> Validation Environment <br /> Testing Environment | 2.05 km x 2.05 km <br /> 1.05 km x 1.05 km <br /> 1.05 km x 1.05 km | 41000 x 41000 px <br /> 21000 x 21000 px <br /> 21000 x 21000 px | 1 <br /> 1 <br /> 1 -->

Item               | Physical Scale    | Resolution      | Number
------------------ | ----------------- | --------------- | ----------
Training Environment   | 2.05 km x 2.05 km  | 41000 x 41000 px | 1 
Validation Environment | 1.05 km x 1.05 km  | 21000 x 21000 px | 1 
Testing Environment    | 1.05 km x 1.05 km  | 21000 x 21000 px | 1 
Surface Images     | 90° x 50.6° FoV   | 1920 x 1080 px  | 2.42 x 10<sup>6</sup>  
Satellite Images   | 50 m x 50 m       | 1000 x 1000 px  | 6.06 x 10<sup>5</sup>
Reprojected Images | 50 m x 50 m       | 1000 x 1000 px  | 6.06 x 10<sup>5</sup>  

The full dataset is approximately 10 TB in size and will be uploaded to [AWS Open Data](https://registry.opendata.aws/).

#### Dataset Generator
Alternatively, we have provided data generation executables [here](https://drive.google.com/open?id=1E7mQR8WfZ94Xu2EetdFzSc3yWuqQ0wyR). 

**Linux:** Run the following in a terminal (``OpenGL`` required): 
```
MyProject area=training overlay=0 offset=0 delay=0.5
```

<!--**Windows:** [unstable] Run the following in a windows command line:
```
MyProject area=training overlay=0 offset=0 delay=0.5
```
-->


**Parameters:**

The ``area`` parameter sets the region from which images will be generated. Note that choosing the ``testing`` option will not generate the additional ground truth metadata, as we welcome users to submit their localization test results to our public benchmark site [https://moonbench.space](https://moonbench.space). 
```
area : training / validation / testing
default: training
``` 

The ``overlay`` parameter hardcodes a unique location ID number in the top left corner of the image. The ID is encoded in binary as adjacent black (0) and white (1) pixels. This parameter provides a direct form of identification from the image itself.
```
overlay : 0 / 1
default: 1
```

To skip the first N locations of the dataset, an ``offset`` can be added. This is useful for data generation on multiple machines.
```
offset: integer
default: 0
```

Depending on the computer hardware, it may be advantageous to set a time ``delay`` (in seconds) between image renders. This will give unreal more time to load the scene and properly render rocks and shadows. 
```
delay: float
default: 0.5
```


#### Dataset Description

##### Simulated Environment
The locations are random (with no repeat) and distributed across distinct training, validation, and testing regions, shown below:

<img src="https://gitlab.com/frontierdevelopmentlab/space-resources/sr2018-localization-public/raw/master/images/dataset_train_smallmap.png" alt="train map" width="500"/> <br />
<img src="https://gitlab.com/frontierdevelopmentlab/space-resources/sr2018-localization-public/raw/master/images/dataset_val_smallmap.png" alt="validate map" width="250"/>
<img src="https://gitlab.com/frontierdevelopmentlab/space-resources/sr2018-localization-public/raw/master/images/dataset_test_smallmap.png" alt="test map" width="250"/>


##### Surface Images
For each location, four rectilinear ground-perspective images (one in each cardinal direction) are generated. 
These are saved as ``MyProject\Saved\Screenshots\images_<area>\images_<location_ID>_<direction>.png``.
- ``<area>`` is one of [training, validation, testing], corresponding to the chosen region
- ``<location_ID>`` is the unique integer ID of the location, starting from 0
- ``<direction>`` is one of [0, 1, 2, 3], corresponding to [North, East, South, West], respectively

<img src="https://wubenjamin.github.io/images/NASA_FDL/HighresScreenshot00005.png" alt="ground 0" width="250"/>
<img src="https://wubenjamin.github.io/images/NASA_FDL/HighresScreenshot00006.png" alt="ground 1" width="250"/> <br /> 
<img src="https://wubenjamin.github.io/images/NASA_FDL/HighresScreenshot00007.png" alt="ground 2" width="250"/>
<img src="https://wubenjamin.github.io/images/NASA_FDL/HighresScreenshot00004.png" alt="ground 3" width="250"/>



##### Metadata

The necessary metadata for each location is also included. 
These are saved as ``MyProject\Saved\Screenshots\coordinates_<region>\<location_ID>_info.txt``, where ``<region>`` and ``<location_ID>`` are described above.
An example metadata file is shown below:
```
XYZ_-146312.0_-110666.0_-2649.625977_RPY_0.0_-15.0_0.0_SunRPY_170.730072_-31.686462_-137.008774_CamHeight_200
```
 - Following ``XYZ`` are the X, Y, and Z ground truth coordinates of the rover, respectively, in Unreal Engine units (UU).
 - Following ``RPY`` are the roll, pitch, and yaw of the camera angle, respectively, in degrees.
 - Following ``SunRPY`` are the roll, pitch, and yaw of the position of the sun, respectively, in degrees.
 - Following ``CamHeight`` is the rover camera height in UU.
 - **Note:** 1 UU = 0.01 meters = 0.2 pixels on the final maps


##### Satellite Images
The ground truth aerial-perspective image for the given location is also provided.
These are extracted from the full region maps. 
Within the dataset, these can be found in ``MyProject\Saved\Aerial\images_<area>\aerial_<location_ID>.png``.

<img src="https://wubenjamin.github.io/images/NASA_FDL/aerial_00000001.png" alt="satellite" width="400"/>

##### Reprojected Images
Additionally included is a processed image that combines the 4 ground-perspective images and reprojects them into a pseudo-aerial view based on camera properties and assuming a flat planetary surface. This reprojection image approximates the same physical scale and resolution as the aerial-perspective images.
Within the dataset, these can be found in ``MyProject\Saved\Reprojections\images_<area>\re_<location_ID>.png``.

<img src="https://wubenjamin.github.io/images/NASA_FDL/re_00000001.png" alt="reprojection" width="400"/>

 
#### Can YOU Localize?
This dataset and a benchmark convolutional neural network localization algorithm, ``PLaNNet (v0)``, are fully described in our paper to be presented at the International Conference on Intelligent Robots and Systems (IROS) 2019. The open source code is provided in this public repository and described below. We warmly welcome all who are interested to improve upon our techniques or develop some of their own!

<!--
---

### Localization tools

This repository contains custom scripts developed for:
1. Data Generation
   - [x] Surface perspective images
   - [x] Orbital maps and stitching
1. Data Processing
   - [ ] Data extraction and cleaning
   - [ ] Reprojections
1. Deep Neural Network
   - [ ] Testing
   - [ ] Training
1. Localization and Benchmarking
   - [ ] Sliding window inferencing
   - [ ] Mapping top matches
   - [ ] Comparison vs. current standards

[**Note:** Pardon our dust! We are currently updating the documentation and cleaning/moving our development code to this public repository.]
-->

---

### Tutorial

We encourage all interested parties to develop their own localization algorithms! 

However, we openly provide our method below to use for benchmarking.

Executing the following steps and running the python scripts in the listed order should enable the user to generate the equivalent processed data, neural network models, and benchmarks as outlined in our IROS 2019 paper.

1. Data Generation
   - [x] Download or [generate](http://moonbench.space/#dataset-generator) surface perspective images
   - [x] [Download](https://drive.google.com/open?id=1Pz-_CSPg-yP9fw27Fgs_h2hC45rhX5i8) orbital maps
1. Data Processing
   - [x] Processing Reprojection Images - ``DataProcessing/batch_reproj.py`` 
   - [x] Processing Aerial Images - ``DataProcessing/batch_aerial.py`` 
1. Deep Neural Network
   - [x] Training - ``DNN/matching/image_match_train.py``
1. Localization and Benchmarking
   - [x] Sliding window inferencing - ``DNN/matching/image_match_predict.py``
   - [x] Mapping top matches - ``Localization/map_best_matches.py``
   - [x] Benchmarking - ``compare_model_distances.py``



---

### Terms of Use

**Using the LUNA Dataset**
The NASA FDL LUNA Localization Dataset is available for unlimited and unrestricted use for any academic, commercial, or governmental use of any kind without fee.

**Redistributing the LUNA Dataset**
You may redistribute, rehost, republish, and mirror the LUNA dataset in any form. However, any use or redistribution of the data must include a citation to the paper and a link to this website (https://www.moonbench.space/).